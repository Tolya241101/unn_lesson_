package second;

import java.util.Scanner;

public class maintwo {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите зарплату в час");
        double num1 = in.nextDouble();
        System.out.println("Введите введите кол-во отработанных часов");
        double num2 = in.nextDouble();
        double moneyInWeek = 0;
        if (num2>40 && num2 < 60 && num1 >= 8) {
            double ost = num2 - 40;
            double plata = num2 - ost;
            ost += ost * 0.5;
            moneyInWeek = num1 * (plata + ost);
        } else if (num2 > 60 && num1 < 8) {
            System.out.println("Несоблюдение условий");
        } else {
            moneyInWeek = num1*num2;
        }
        System.out.println("Зарплата в неделю: " + moneyInWeek);
    }
}
